import * as faker from "faker";
import { FirstStepFillProps, SecondStepFillProps } from "../components/types/signUp";

export default class SignupDataGenerator {
  firstStep() : FirstStepFillProps {
    return {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      password: this.password(),
      companyName: faker.company.companyName(),
      companyType: "Ad network",
      country: "Albania",
    };
  }

  secondStep() : SecondStepFillProps {
    return {
      imName: faker.internet.userName(),
      countries: this.countries(),
      trafficType: "Network",
      messenger: "Telegram",
      revenue: "1000$-2000$",
      howFoundOut: { option: "Conference", subOption: "CPAconf" },
    };
  }

  /*
   * If "stable" set to true (default)  return persisted password
  */
  private password(stable: boolean = true) : string {
    return stable ? "asghNTLP12T" : faker.internet.password(6);
  }

  private countries(number : number = 3) {
    return faker.fake(`${"{{address.country}}, ".repeat(number).slice(0, -2)}`);
  }
}
