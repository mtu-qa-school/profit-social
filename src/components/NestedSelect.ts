import { WebElement } from "school-helper/lib/playwright";
import { Page } from "playwright";

export default class NestedSelect {
  private root : WebElement;

  private popup : WebElement;

  private selectElement: WebElement;

  constructor(protected page: Page, selector: string) {
    this.root = new WebElement(page, selector);
    this.selectElement = this.root.getChildElement("/div[contains(@class, 'select')]");
    this.popup = this.selectElement.getChildElement("//div[contains(@class,'select__dropdown')]");
  }

  async select(option: string, subOption? : string): Promise<void> {
    await this.selectElement.click();
    await this.popup.waitForElement(4000);
    let optionElement;

    if (subOption?.length) {
      optionElement = await this.getOptionFromNestedList(option, subOption);
    }
    else {
      optionElement = this.getOptionByElementWithText(option);
    }

    return optionElement.click();
  }

  private async getOptionFromNestedList(option: string, subOption? : string) : Promise<WebElement> {
    const elem = this.getOptionByElementWithText(option);
    await elem.click();
    return this.popup.getChildElement(`//div[text() = '${subOption}']`);
  }

  private getOptionByElementWithText(option: string): WebElement {
    return this.popup.getChildElement(`//div[text() = '${option}']`);
  }
}
