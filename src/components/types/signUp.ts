export type FirstStepFillProps = {
  firstName : string,
  lastName : string,
  email : string,
  password : string,
  companyName : string,
  companyType : string,
  country: string;
};

export type SecondStepFillProps = {
  countries : string,
  imName : string,
  messenger : string,
  trafficType : string,
  revenue : string,
  howFoundOut : {
    option : string,
    subOption?: string
  },
};
