import { Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";
import Select from "../../../components/Select";
import AbstractStep from "./AbstractStep";
import NestedSelect from "../../../components/NestedSelect";
import { SecondStepFillProps } from "../../../components/types/signUp";
import { SecondStepXPaths } from "../constants/signUpXPaths";

export default class SecondStepSignUp extends AbstractStep {
  private messengerSelect : Select;

  private trafficTypeSelect : Select;

  private revenueSelect : Select;

  private imInput : WebElement;

  private strongestGEOInput : WebElement;

  private howFoundOutSelect : NestedSelect;

  private termsCheckbox : WebElement;

  private policyCheckbox : WebElement;

  constructor(protected page : Page) {
    super(page);
    this.messengerSelect = new Select(this.page, SecondStepXPaths.MESSENGER);
    this.trafficTypeSelect = new Select(this.page, SecondStepXPaths.TRAFFIC_TYPE);
    this.revenueSelect = new Select(this.page, SecondStepXPaths.REVENUE);
    this.howFoundOutSelect = new NestedSelect(this.page, SecondStepXPaths.HOW_FOUND_OUT);
    this.imInput = new WebElement(this.page, SecondStepXPaths.IM_NAME);
    this.strongestGEOInput = new WebElement(this.page, SecondStepXPaths.STRONGEST_GEO);
    this.termsCheckbox = new WebElement(this.page, SecondStepXPaths.TERMS);
    this.policyCheckbox = new WebElement(this.page, SecondStepXPaths.POLICY);
  }

  async fill(props : SecondStepFillProps) : Promise<void> {
    await this.enterAuthData(props);
  }

  private async enterAuthData({
    imName,
    countries,
    messenger,
    trafficType,
    revenue,
    howFoundOut,
  } : SecondStepFillProps) : Promise<void> {
    await this.messengerSelect.select(messenger);
    await this.imInput.userType(imName);
    await this.trafficTypeSelect.select(trafficType);
    await this.revenueSelect.select(revenue);
    await this.strongestGEOInput.type(countries);
    await this.howFoundOutSelect.select(howFoundOut.option, howFoundOut.subOption);
    await this.policyCheckbox.click();
    await this.termsCheckbox.click();
  }
}
