import { Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";
import Select from "../../../components/Select";
import AbstractStep from "./AbstractStep";
import NestedSelect from "../../../components/NestedSelect";
import { FirstStepFillProps } from "../../../components/types/signUp";
import { FirstStepXPaths } from "../constants/signUpXPaths";

export default class FirstStepSignUp extends AbstractStep {
  private signupButtonTop : WebElement;

  private firstNameInput : WebElement;

  private lastNameInput : WebElement;

  private emailInput : WebElement;

  private passwordInput : WebElement;

  private confirmPasswordInput : WebElement;

  private companyNameInput : WebElement;

  private countrySelect : Select;

  private companyTypeSelect : NestedSelect;

  private nextStepButton : WebElement;

  constructor(protected page : Page) {
    super(page);
    this.signupButtonTop = new WebElement(this.page, FirstStepXPaths.SIGNUP_BTN);
    this.firstNameInput = new WebElement(this.page, FirstStepXPaths.FIRST_NAME);
    this.lastNameInput = new WebElement(this.page, FirstStepXPaths.LAST_NAME);
    this.emailInput = new WebElement(this.page, FirstStepXPaths.EMAIL);
    this.passwordInput = new WebElement(this.page, FirstStepXPaths.PASS);
    this.confirmPasswordInput = new WebElement(this.page, FirstStepXPaths.CONFIRM_PASS);
    this.companyNameInput = new WebElement(this.page, FirstStepXPaths.COMPANY_NAME);
    this.companyTypeSelect = new NestedSelect(this.page, FirstStepXPaths.COMPANY_TYPE);
    this.countrySelect = new Select(this.page, FirstStepXPaths.COUNTRY);
    this.nextStepButton = new WebElement(this.page, FirstStepXPaths.NEXT_STEP);
  }

  async fill(props : FirstStepFillProps) : Promise<void> {
    await this.goToForm();
    await this.enterAuthData(props);
    return this.goToNextStep();
  }

  private async goToForm() : Promise<void> {
    return this.signupButtonTop.click();
  }

  private async enterAuthData({
    firstName, lastName, email, password, companyName, companyType, country,
  } : FirstStepFillProps) : Promise<void> {
    await this.firstNameInput.userType(firstName);
    await this.lastNameInput.userType(lastName);
    await this.emailInput.userType(email);
    await this.passwordInput.userType(password);
    await this.confirmPasswordInput.userType(password);
    await this.companyNameInput.userType(companyName);
    await this.companyTypeSelect.select(companyType);
    await this.countrySelect.select(country);
  }

  private async goToNextStep() : Promise<void> {
    await this.nextStepButton.click();
  }
}
