import { Page } from "playwright";
import { FirstStepFillProps, SecondStepFillProps } from "../../../components/types/signUp";

export default abstract class AbstractStep {
  constructor(protected page : Page) {
  }

  abstract fill(props : FirstStepFillProps | SecondStepFillProps): Promise<void>;
}
