import { Browser, Page } from "playwright";
import FirstStepSignUp from "./authSteps/firstStepSignUp";
import SecondStepSignUp from "./authSteps/secondStepSignUp";
import SignupDataGenerator from "../../helpers/SignupDataGenerator";

export default class ProfitSocialPO {
  constructor(public page : Page) {
  }

  async signUp() : Promise<void> {
    const signUpData = new SignupDataGenerator();
    const firstStepData = signUpData.firstStep();
    await new FirstStepSignUp(this.page).fill(firstStepData);
    const secondStepData = signUpData.secondStep();
    return new SecondStepSignUp(this.page).fill(secondStepData);
  }

  static async initPage(browser : Browser) : Promise<ProfitSocialPO> {
    const page = await browser.newPage();
    await page.goto("https://www.profitsocial.com/en");
    return new ProfitSocialPO(page);
  }
}
