export enum FirstStepXPaths {
  SIGNUP_BTN = "//div[@data-slideto='signup'][contains(@class, 'button')]",
  FIRST_NAME = "//input[@id='firstName']",
  LAST_NAME = "//input[@id='lastName']",
  EMAIL = "//input[@id='email']",
  PASS = "//input[@id='password']",
  CONFIRM_PASS = "//input[@name='confirmation']",
  COMPANY_NAME = "//input[@name='companyName']",
  COMPANY_TYPE = "//input[@name='companyType']/parent::*",
  COUNTRY = "//*[@id = 'country']/parent::*",
  NEXT_STEP = "//div[contains(@class, 'signup-form__next')]"
}

export enum SecondStepXPaths {
  MESSENGER = "//*[@name= 'imType']/parent::*",
  TRAFFIC_TYPE = "//*[@name= 'trafficType']/parent::*",
  IM_NAME = "//input[@name= 'imName']",
  REVENUE = "//*[@name= 'revenue']/parent::*",
  STRONGEST_GEO = "//input[@name= 'topCountries']",
  HOW_FOUND_OUT = "//*[@name= 'howFoundOut']/parent::*",
  TERMS = "//label[@for='terms-input']",
  POLICY = "//label[@for='policy-input']"
}
