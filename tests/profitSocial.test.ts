import { Browser, chromium, LaunchOptions } from "playwright";
import ProfitSocialPO from "../src/pages/profitSocial/ProfitSocialPO";

let browser : Browser;
jest.setTimeout(310000);

describe("Profit Social test", () => {
  afterEach(async () => {
    await browser.close();
  });

  it("should signup", async () => {
    const launchOptions : LaunchOptions = {
      headless: false,
    };
    browser = await chromium.launch(launchOptions);
    const page = await ProfitSocialPO.initPage(browser);
    await page.signUp();
    await page.page.waitForTimeout(20000);
  });
});
